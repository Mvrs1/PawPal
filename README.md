# PawPal

> _This project was part of our submission at [SF HACKS 2020](https://devpost.com/software/pawpal)_

A friendly app for pet owners to track the health of their pets

## Current State

### (2021-07-15)

- As of right now i'll be finishing up the UI (meaning all the screens)
- I will add my own touch to the designs of [@essveeee](https://github.com/essveeee)
- The goal of finishing out the UI is to test my skills and expand my knowledge to transfer to any role in frontend
- Possible to add a backend soon. Most likely Postgres and NestJS
